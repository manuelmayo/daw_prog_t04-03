//T04_3_CuentaBancaria

import java.io.IOException;
import java.util.Scanner;

public class AplicacionCuentaBancaria {
	
	private static Scanner ENTRADA = new Scanner (System.in);
	
	public static void clear(){
	    try {
	        if (System.getProperty("os.name").contains("Windows"))
	            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
	        else
	            Runtime.getRuntime().exec("clear");
	    } catch (IOException | InterruptedException ex) {}
	}
	
	public static void pressEnter() {
		System.out.print("\nPulse ENTER para continuar...");
		ENTRADA.nextLine();
	}
	
	public static void menu() {
		clear();
		System.out.printf("%-20sGESTI�N DE CUENTA BANCARIA%20s\n\n","[","]");
		System.out.println("1.  Ver el n�mero de cuenta completo "
				+ "(CCC - C�digo Cuenta Cliente)");
		System.out.println("2.  Ver el titular de la cuenta");
		System.out.println("3.  Ver el c�digo de la entidad");
		System.out.println("4.  Ver el c�digo de la oficina");
		System.out.println("5.  Ver el n�mero de la cuenta");
		System.out.println("6.  Ver los d�gitos de control de la cuenta");
		System.out.println("7.  Realizar un ingreso");
		System.out.println("8.  Retirar efectivo.");
		System.out.println("9.  Consultar saldo");
		System.out.println("10. Salir de la aplicaci�n");
	}
	
	public static void gestionMenu(CuentaBancaria cuenta) {
		boolean programaOn = true;
		while (programaOn) {
			menu();
			System.out.print("\nOpci�n: ");
			String opcion = ENTRADA.nextLine();
			System.out.println();
			switch (opcion) {
				case "1": 
					cuenta.infoCCC();
					pressEnter();
					break;
				case "2":
					cuenta.infoTitular();
					pressEnter();
					break;
				case "3":
					cuenta.infoEntidad();
					pressEnter();
					break;
				case "4":
					cuenta.infoOficina();
					pressEnter();
					break;
				case "5":
					cuenta.infoCuenta();
					pressEnter();
					break;
				case "6":
					cuenta.infoDigitosControl();
					pressEnter();
					break;
				case "7":
					System.out.print("Cantidad a ingresar: ");
					double cantidad_ingreso = ENTRADA.nextDouble();
					ENTRADA.nextLine();
					cuenta.ingresar(cantidad_ingreso);
					pressEnter();
					break;
				case "8":
					System.out.print("Cantidad a retirar: ");
					double cantidad_retirar = ENTRADA.nextDouble();
					ENTRADA.nextLine();
					cuenta.retirar(cantidad_retirar);
					pressEnter();
					break;
				case "9":
					cuenta.infoSaldo();
					pressEnter();
					break;
				case "10":
					programaOn = false;
					System.out.println("Adi�s!");
					break;
				default:
					System.out.println("Opci�n inv�lida");
			}
		}
	}
	
	public static void main(String[] args) {
		String titular = "Manuel Mayo Lado";
		String CCC = "6745 5921 58 9246419572";
		CuentaBancaria cuenta = new CuentaBancaria(titular, CCC);
		gestionMenu(cuenta);
	}
		
}
