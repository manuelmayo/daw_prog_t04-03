//T04_3_CuentaBancaria

import java.lang.IllegalArgumentException;

public class CuentaBancaria {
	
	public String titular;
	public String codigo;
	public String entidad;
	public String oficina;
	public String control;
	public String cuenta;
	public double saldo = 0;
	
	public CuentaBancaria(String titular, String codigo) {
		if (titular.length() > 3 && titular.length() < 30) {
			this.titular = titular;
		} else {
			String error_msx = "El titular debe tener m�s de 3 caracteres y menos de 30";
			throw new IllegalArgumentException(error_msx);
		}
		if (checkDC(codigo)) {
			this.codigo = codigo.replace(" ","");
			this.entidad = this.codigo.substring(0, 4);
			this.oficina = this.codigo.substring(4, 8);
			this.control = this.codigo.substring(8, 10);
			this.cuenta = this.codigo.substring(10, 19);
		} else {
			String error_msx = String.format("%s No es un c�digo v�lido",codigo);
			throw new IllegalArgumentException(error_msx);
		}
	}
	
	public void ingresar(double money) {
		this.saldo += money;
	}
	
	public void retirar(double money) {
		if (money <= this.saldo) {
			this.saldo -= money;
		} else {
			System.out.println("Saldo insuficiente");
		}
	}
	
	public void infoSaldo() {
		System.out.printf("Su saldo actual es de %s euros\n", this.saldo);
	}
	
	public void infoEntidad() {
		System.out.printf("Su n�mero de Entidad Bancaria es %s\n", this.entidad);
	}
	
	public void infoOficina() {
		System.out.printf("Su n�mero de Oficina es %s\n", this.oficina);
	}
	
	public void infoCuenta() {
		System.out.printf("Su n�mero de Cuenta Bancaria es %s\n", this.cuenta);
	}
	
	public void infoDigitosControl() {
		System.out.printf("Sus Digitos de Control son %s\n", this.control);
	}
	
	public void infoCCC() {
		System.out.printf("Su n�mero de Cuenta Completo (CCC) es %s\n", this.codigo);
	}
	
	public void infoTitular() {
		System.out.printf("El Titular es %s\n", this.titular);
	}
	
	private boolean checkDC(String codigo) {
		codigo = codigo.replace(" ", "");
		if (codigo.length() == 20) {
			String entidad = codigo.substring(0, 4);
			String oficina = codigo.substring(4, 8);
			String control = codigo.substring(8, 10);
			String cuenta = codigo.substring(10, 20);
			int dControl_1 = digitoControl_1(entidad,oficina);
			int dControl_2 = digitoControl_2(cuenta);
			String dControl = String.valueOf(dControl_1)+String.valueOf(dControl_2);
			if (dControl.equals(control)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	private int digitoControl_1(String entidad, String oficina) {
		int sumaEntidad = 0;
		int sumaOficina = 0;
		int[] mulEntidad = new int[] {4,8,5,10};
		int[] mulOficina = new int[] {9,7,3,6};
		for (int i=0;i<4;i++) {
			sumaEntidad += Integer.parseInt(entidad.substring(i,i+1))*mulEntidad[i];
			sumaOficina += Integer.parseInt(oficina.substring(i,i+1))*mulOficina[i];
		}
		int sumaTotal = sumaEntidad + sumaOficina;
		int digitoControl = 11-(sumaTotal%11);
		if (digitoControl >= 10) {
			digitoControl = 11-digitoControl;
		}
		return digitoControl;
	}
	
	private int digitoControl_2(String cuenta) {
		int[] mulCuenta = new int[] {1,2,4,8,5,10,9,7,3,6};
		int sumaCuenta = 0;
		for (int i=0;i<10;i++) { 
			sumaCuenta += Integer.parseInt(cuenta.substring(i,i+1))*mulCuenta[i];
		}
		int digitoControl = 11-(sumaCuenta%11); 
		if (digitoControl >= 10) {
			digitoControl = 11-digitoControl;
		}
		return digitoControl;
	}
	
}
